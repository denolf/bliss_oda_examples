> :warning: These notebooks use the deprecated Bliss 1 API before the introduction of blissdata

## Installation
Create a [BLISS environment](https://bliss.gitlab-pages.esrf.fr/bliss/master/installation.html)
```bash
conda create --name bliss_oda_env
conda activate bliss_oda_env

conda config --env --set channel_priority false
conda config --env --add channels conda-forge
conda config --env --append channels defaults
conda config --env --append channels esrf-bcu
conda config --env --append channels tango-controls

conda install --channel esrf-bcu bliss
```

Register the BLISS environment as an ipython kernel:
```bash
conda activate bliss_oda_env
conda install ipykernel
python -m ipykernel install --user --name=bliss_oda_env
```

### Modify the kernel configuration (NOT NEEDED ANYMORE)

Modify the kernel configuration file `${HOME}/.local/share/jupyter/kernels/bliss/kernel.json`
```json
{
 "argv": [
  ".../bin/python",
  "-m",
  "ipykernel_launcher",
  "-f",
  "{connection_file}"
 ],
 "display_name": "bliss",
 "language": "python"
}
```
to support gevent
```json
{
 "argv": [
  ".../bin/python",
  "-c",
  "import gevent; from gevent.monkey import patch_all; patch_all(thread=False); import aiogevent; import asyncio;asyncio.set_event_loop_policy(aiogevent.EventLoopPolicy()); from ipykernel.kernelapp import main; gevent.spawn(main).get()",
  "-f",
  "{connection_file}"
 ],
 "display_name": "bliss",
 "language": "python"
}
```

## Run the examples
Start the servers for the BLISS demo session (Beacon, Tango database, simulated device servers, demo configuration)
```bash
conda activate bliss_oda_env
git clone https://gitlab.esrf.fr/bliss/bliss
cd bliss/
conda install --file ./requirements-test.txt

python demo/start_demo_servers.py
```

Start the BLISS command line interface to the demo session
```bash
conda activate bliss_oda_env
TANGO_HOST=$(hostname):10000 BEACON_HOST=$(hostname):10001 bliss -s demo_session
```

Launch jupyterlab to run the notebooks
```bash
jupyter lab
```

### Notebooks:
Illustrate different ways to do online data analysis with BLISS

1. _RedisNodeEvents_: ODA without experiment feedback using the low-level API
2. _RedisScansWatcher_: ODA without experiment feedback using the high-level API
3. _ODA FFT_: Example of using the high-level API
4. _ODA feedback beacon channels_: ODA with custom experiment feedback
5. _Bliss as a library_: Full control of ODA and acquisition (in the same process)

Currently BLISS has no "server mode" that provides full acquisition control like you have through the CLI.

### Presentations:
https://slides.com/matiasg/blissoda
