"""Bliss user script to perform and experiment with feedback
from online data analysis (ODA).
"""

import gevent
from bliss.controllers.lima.roi import Roi as LimaRoi
from bliss.config.channels import Channel
from bliss.common.logtools import user_print


def experiment_with_oda_feedback(timeout=10):
    if "fullroi" not in beamviewer.roi_counters:
        beamviewer.roi_counters["fullroi"] = LimaRoi(0, 0, 1023, 1023)
    counter = beamviewer.counter_groups.fullroi.fullroi_avg.fullname

    odastate = {"counter": counter, "is_oda_feedback": False, "stop": False}
    odafinished = gevent.event.Event()

    def send():
        odastate["is_oda_feedback"] = False
        odachannel.value = odastate

    def receive(value):
        if not value.get("is_oda_feedback"):
            return
        odastate.update(value)
        odafinished.set()

    odachannel = Channel("oda_feedback", callback=receive, default_value=odastate)
    try:
        while not odastate["stop"]:
            send()
            s = ascan(slit_vertical_offset, -2, 2, 30, 0.001, beamviewer, save=False)
            user_print("Waiting for ODA feedback ...")
            assert odafinished.wait(timeout=timeout), "ODA not responding"
            odafinished.clear()
    finally:
        odastate["stop"] = True
        send()
